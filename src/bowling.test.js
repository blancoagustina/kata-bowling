const expect = require('chai').expect;
const Bowling = require('./bowling');


describe('score', () => {
  // El peor juego del universo
  // Que tire en todos los tiros siempre 1
  // Que haya al menos un spare
  // Que haya al menos un strike
  // Que haya un spare y un strike
  // Que haya todos spare
  // El juego perfecto

  // El peor juego del universo
  it('El peor juego del universo', () => {
    const bowling = new Bowling();
    for (let i = 0; i < 20; i++) {
      bowling.tirar(0)
    }
    const score = bowling.score();
    expect(score).to.equal(0);
  });

  // Que tire en todos los tiros siempre 1
  it('Que tire en todos los tiros siempre 1', () => {
    const bowling = new Bowling();
    for (let i = 0; i < 20; i++) {
      bowling.tirar(1)
    }
    const score = bowling.score();
    expect(score).to.equal(20);
  });

  // Que haya al menos un spare
  it('Que haya al menos un spare', () => {
    const bowling = new Bowling();
    bowling.tirar(5);
    bowling.tirar(5);
    for (let i = 2; i < 20; i++) {
      bowling.tirar(1)
    }
    const score = bowling.score();
    expect(score).to.equal(29);
  });

  // Que haya al menos un strike
  it('Que haya al menos un strike', () => {
    const bowling = new Bowling();
    bowling.tirar(10);
    bowling.tirar(0);
    for (let i = 2; i < 20; i++) {
      bowling.tirar(1)
    }
    const score = bowling.score();
    expect(score).to.equal(30);
  });

  // Que haya un spare y un strike
  it('Que haya un spare y un strike', () => {
    const bowling = new Bowling();
    bowling.tirar(10);
    bowling.tirar(0);
    bowling.tirar(5);
    bowling.tirar(5);
    for (let i = 4; i < 20; i++) {
      bowling.tirar(1)
    }
    const score = bowling.score();
    expect(score).to.equal(47);
  });

  // Que haya todos spare
  it('Que haya todos spare', () => {
    const bowling = new Bowling();
    for (let i = 0; i < 21; i++) {
      bowling.tirar(5)
    }
    const score = bowling.score();
    expect(score).to.equal(150);
  });

  // El juego perfecto
  it('El juego perfecto', () => {
    const bowling = new Bowling();
    for (let i = 0; i < 10; i++) {
      bowling.tirar(10);
      bowling.tirar(null);
    }
    bowling.tirar(10);
    bowling.tirar(10);
    const score = bowling.score();
    expect(score).to.equal(300);
  });
});