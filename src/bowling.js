import Turno from './turno.js';

export default class Bowling {
  scores = [];

  tirar(pinos) {
    const ultimo = this.scores[this.scores.length-1];
    if (!ultimo || ultimo.segundoTiro !== undefined) {
      this.scores.push(new Turno(pinos));
    } else {
      ultimo.segundoTiro = pinos;
    }
  }

  score() {
    return this.scores.reduce((acumulado, actual, indiceActual) => {
      // Para que no cuente 2 veces el turno 11, si hay
      if (indiceActual >= 10) {
        return acumulado;
      }

      if (actual.isStrike()) {
        const proximo1 = this.scores[indiceActual+1];
        const proximo2 = this.scores[indiceActual+2];
        let strikeScore = proximo1.getScore();
        if (proximo2 && proximo1.isStrike()) {
          if (proximo2.isStrike()) {
            strikeScore += 10;
          } else {
            strikeScore += proximo2.primerTiro;
          }
        }
        return acumulado + actual.getScore() + strikeScore;
      }

      if (actual.isSpare()) {
        const proximo1 = this.scores[indiceActual+1];
        return acumulado + actual.getScore() + proximo1.primerTiro;
      }

      // No es strike ni spare
      return acumulado + actual.getScore();
    },
    0); //valor inicial
  }
}

module.exports = Bowling;