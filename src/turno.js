export default class Turno {
    primerTiro;
    segundoTiro;

    constructor(primerTiro) {
        this.primerTiro = primerTiro;
    }

    getScore() {
        return this.primerTiro + this.segundoTiro;
    }
    isSpare(){
        return this.primerTiro + this.segundoTiro === 10;
    }
    isStrike(){
        return this.primerTiro === 10 || this.segundoTiro === 10;
    }
}

module.exports = Turno;
